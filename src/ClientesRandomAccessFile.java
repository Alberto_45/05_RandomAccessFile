import javax.swing.*;
import java.io.IOException;

public class ClientesRandomAccessFile {

    //<editor-fold desc="Variables globales">
    private static final String ficheroCliente = "clientes.dat";
    private static final Cliente cliente = new Cliente();
    //</editor-fold>

    //<editor-fold desc="Main">
    public static void main(String[] args) {
        menuPrincipal();
    }
    //</editor-fold>

    //<editor-fold desc="Menu principal">
    private static void menuPrincipal() {
        int opcion;

        try {
            do {
                opcion = Integer.parseInt(JOptionPane.showInputDialog(null, """
                        ****** MENU PRINCIPAL *******

                        1.-Alta cliente
                        2.-Busqueda cliente
                        3.-Salir

                        Elije una opcion"""));

                switch (opcion) {
                    case 1 -> altaCliente();
                    case 2 -> buscaqueCliente();
                    case 3 -> System.exit(0);
                    default -> JOptionPane.showMessageDialog(null, "Opcion incorrecta", "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            } while ((opcion < 1 || opcion > 3));
        } catch (NumberFormatException | IOException e) {
            JOptionPane.showMessageDialog(null, "Opcion incorrecta", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        menuPrincipal();
    }
    //</editor-fold>

    //<editor-fold desc="Alta cliente">
    public static void altaCliente() throws IOException {

        // generamos codigo cliente
        cliente.setCodigo(ficheroCliente);
        //obtenemos el codigo cliente
        int codigoCliente = cliente.getCodigo();

        //introducimos los datos del cliente
        System.out.println("Codigo cliente: " + codigoCliente);

        //introducimos nombre
        cliente.setNombre();
        //obtenemos el nombre
        String nombre = cliente.getNombre();

        //introducimso el sueldo
        cliente.setSueldo();

        //obtenemos el sueldo
        long saldo = cliente.getSueldo();

        //guardamos los datos del cliente en un fichero
        cliente.guardaDatosClienteEnFicheroBinario(ficheroCliente, codigoCliente, nombre, saldo);
    }
    //</editor-fold>

    //<editor-fold desc="Busqueda de clientes">
    public static void buscaqueCliente() throws IOException {

        cliente.buscarCliente(ficheroCliente);

        menuPrincipal();
    }
    //</editor-fold>

}
